var searchData=
[
  ['msg_5ferror_5fbad_5fbackup_5ffile',['MSG_ERROR_BAD_BACKUP_FILE',['../tools_8hpp.html#a619c0cdc2b3006bc83ebd37d012ad834',1,'tools.hpp']]],
  ['msg_5ferror_5finvalid_5fargument',['MSG_ERROR_INVALID_ARGUMENT',['../tools_8hpp.html#aeb1d4a62aa031489d50bfb7727c47437',1,'tools.hpp']]],
  ['msg_5ferror_5finvalid_5fnumber',['MSG_ERROR_INVALID_NUMBER',['../tools_8hpp.html#a29e1d1f71d0703411c3f248830fd2f23',1,'tools.hpp']]],
  ['msg_5ferror_5fmissing_5farg_5fspecifier',['MSG_ERROR_MISSING_ARG_SPECIFIER',['../tools_8hpp.html#a0b004a2402ae717924cb6a0a0079bff5',1,'tools.hpp']]],
  ['msg_5ferror_5fno_5flists_5fwere_5ffound',['MSG_ERROR_NO_LISTS_WERE_FOUND',['../tools_8hpp.html#aa5cf87cbcdc050ef243ec58cfef161f9',1,'tools.hpp']]],
  ['msg_5ferror_5fnonsense_5fbase_5flist',['MSG_ERROR_NONSENSE_BASE_LIST',['../tools_8hpp.html#a7994587c90dbbdc7bddbd91e76abef61',1,'tools.hpp']]],
  ['msg_5ferror_5frepeated_5fargument',['MSG_ERROR_REPEATED_ARGUMENT',['../tools_8hpp.html#a28d837992ca88b628d7f28d1086c517d',1,'tools.hpp']]],
  ['msg_5ferror_5ftoo_5ffew_5felems',['MSG_ERROR_TOO_FEW_ELEMS',['../tools_8hpp.html#a3722c4b856a343439e76a95ae11b7fb2',1,'tools.hpp']]],
  ['msg_5ferror_5funable_5fto_5fload_5ffile',['MSG_ERROR_UNABLE_TO_LOAD_FILE',['../tools_8hpp.html#a804e185d47b6823905181ae9d1b5364a',1,'tools.hpp']]],
  ['msg_5ferror_5funreached_5fmin',['MSG_ERROR_UNREACHED_MIN',['../tools_8hpp.html#a58415cdce7af0a66926ab685c1ffc6a2',1,'tools.hpp']]],
  ['msg_5fnothing_5fspared',['MSG_NOTHING_SPARED',['../tools_8hpp.html#abe1af918cd546a8cc101ec6703c41598',1,'tools.hpp']]],
  ['msg_5fwarning_5finvalid_5fid',['MSG_WARNING_INVALID_ID',['../tools_8hpp.html#a1a54b2f2bcf7b36b89c3452a94108a9a',1,'tools.hpp']]]
];
