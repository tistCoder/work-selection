var searchData=
[
  ['random_5findex',['random_index',['../tools_8cpp.html#a62fbc0b24e4a1a8cff67dcc838a7a07a',1,'random_index(size_t zerotonm1):&#160;tools.cpp'],['../tools_8hpp.html#a62fbc0b24e4a1a8cff67dcc838a7a07a',1,'random_index(size_t zerotonm1):&#160;tools.cpp']]],
  ['remove_5faggregated_5ftail_5fin_5fplace',['remove_aggregated_tail_in_place',['../tools_8hpp.html#ab792ffe6d538f5318efb635d3c191694',1,'tools.hpp']]],
  ['replace_5fsuffix_5fin_5fplace',['replace_suffix_in_place',['../tools_8cpp.html#a539f9240e0115ddc5763ee4a9a0273e7',1,'replace_suffix_in_place(std::string &amp;str, size_t pos, const std::string &amp;new_suffix):&#160;tools.cpp'],['../tools_8cpp.html#a823f95ed9021254477ed13c60d24b75d',1,'replace_suffix_in_place(std::string &amp;str, size_t pos, std::string &amp;&amp;new_suffix):&#160;tools.cpp'],['../tools_8hpp.html#a6f395854c648a9d2cef87b98d0740b83',1,'replace_suffix_in_place(std::string &amp;str, size_t start, const std::string &amp;new_suffix):&#160;tools.cpp'],['../tools_8hpp.html#afd0a808f4e4be9264cdb954d11cc1b30',1,'replace_suffix_in_place(std::string &amp;str, size_t start, std::string &amp;&amp;new_suffix):&#160;tools.cpp']]],
  ['requires_5floading',['requires_loading',['../classsession.html#a1264a75a18a30b1d7e22ebec363e29e9',1,'session']]],
  ['reset',['reset',['../classsession.html#af025491bff7ee1f168af5c798098ab31',1,'session']]],
  ['rtrim_5fstr',['rtrim_str',['../tools_8cpp.html#ab347c759ec78ed0673ccad25dd5e2460',1,'rtrim_str(std::string &amp;str):&#160;tools.cpp'],['../tools_8hpp.html#ab347c759ec78ed0673ccad25dd5e2460',1,'rtrim_str(std::string &amp;str):&#160;tools.cpp']]]
];
