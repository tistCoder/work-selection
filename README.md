# Work Selection

Since v1.0.0.0 it allows an automized selection from a given pool of people, thus they are randomly
selected and only chosen from the pool if they attend the institution to that point of time.

v2.0.0.0, in turn, was completely rewritten and provided an iterator-based election
instead of a rearranging set of maps. The UI was adjusted then and works well with
the map as well as the vector implementation.

v3.0.0.0 (which once again was rewritten) now allows *repeatable elections* within one execution and adds *sessions* which are based on
a list of people (there no longer is just one) as well as save who has been selected before. For instance,
you have the same array of people in various courses, but each course shall be treated independently.

**Depencies**
- Botan 2.13+: https://botan.randombit.net

Future: Encrypted lists, translations and a GUI.