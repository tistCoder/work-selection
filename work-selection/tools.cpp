#include "include/tools.hpp"

/** @file */

void fatal_error(const std::string& msg, int exit_code) {
	set_color(COLOR_LIGHT_RED, std::cerr);
	std::cerr << msg << std::endl;
	set_color(COLOR_NORMAL, std::cerr);
	exit(exit_code);
	return;
}

//checks if a given char is a number
bool is_ASCII_number(char c) {
    return (c >= '0') && (c <= '9');
}

//checks if a given string is a number
bool is_ASCII_number(const std::string& str) {
	//vector-wise check
	for (auto i = str.cbegin(); i != str.cend(); i++) {
		if (!is_ASCII_number(*i)) {
			return false;
		}
	}
	return true;
}

#ifdef WIN32
	//sets the printing-color in a windows-terminal
	void set_color(uint8_t color, std::ostream& os) {
		if (color < 0) {
			color *= -1; //make a negative number positive
		}
		//sets the color of the terminal-stream
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);

		return;
	}

	void printc(const std::string& msg, uint8_t color) {
		set_color(color);
		std::cout << msg;
		set_color(COLOR_NORMAL);
		return;
	}

	void printlnc(const std::string& msg, uint8_t color) {
		set_color(color);
		std::cout << msg;
		set_color(COLOR_NORMAL);
		std::cout << std::endl;
		return;
	}

#else
	void set_color(const char* ansi_escape_char, std::ostream& os) {
		os << ansi_escape_char;
		return;
	}

	void printc(const std::string& msg, const char* ansi_escape_char) {
		set_color(ansi_escape_char, std::cout);
		std::cout << msg;
		set_color(COLOR_NORMAL, std::cout);
		return;
	}

	void printlnc(const std::string& msg, const char* ansi_escape_char) {
		set_color(ansi_escape_char, std::cout);
		std::cout << msg;
		set_color(COLOR_NORMAL, std::cout);
		std::cout << std::endl;
		return;
	}

#endif

void warning(const std::string& msg) {

	set_color(COLOR_YELLOW, std::cerr);
	std::cerr << msg << std::endl;
	set_color(COLOR_NORMAL, std::cerr);

	return;
}

bool has_prefix(const std::string& str, const std::string& prefix) {
	//a prefix cannot be longer than the entire phrase
	if(str.size() < prefix.size()) {
		return false;
	}

	//check from begins to prefix' end
	for(auto i = str.cbegin(), j = prefix.cbegin(); j != prefix.cend(); i++, j++) {
		if(*i != *j) {
			return false;
		}
	}
	return true;
}

size_t _locate_suffix_begin_in_str(const std::string& str, const std::string& suffix) {
	//a suffix cannot be longer than the entire phrase
	if(str.size() < suffix.size()) {
		return std::string::npos;
	}

	//check from reverse begins to suffix' reverse end
	size_t pos = str.size()-1;
	for(auto j = suffix.crbegin(); j != suffix.crend(); pos--, j++) {
		if(str[pos] != *j) {
			return std::string::npos;
		}
	}
	return pos;
}

bool has_suffix(const std::string& str, const std::string& suffix) {
	return _locate_suffix_begin_in_str(str, suffix) != std::string::npos;
}

bool has_suffix_and_replace_if(std::string& str, const std::string& suffix, const std::string& new_suffix) {
	size_t pos = _locate_suffix_begin_in_str(str, suffix);
	if(pos != std::string::npos) {
		replace_suffix_in_place(str, pos, new_suffix);
		return true;
	}
	return false;
}

bool has_suffix_and_replace_if(std::string& str, const std::string& suffix, std::string&& new_suffix) {
	size_t pos = _locate_suffix_begin_in_str(str, suffix);
	if(pos != std::string::npos) {
		replace_suffix_in_place(str, pos, std::move(new_suffix));
		return true;
	}
	return false;
}

void replace_suffix_in_place(std::string& str, size_t pos, const std::string& new_suffix) {
	str.resize(pos+1);
	str += new_suffix;
	return;
}

void replace_suffix_in_place(std::string& str, size_t pos, std::string&& new_suffix) {
	str.resize(pos+1);
	str += std::move(new_suffix);
	return;
}

void ltrim_str(std::string& str) {
	//erase from left to right as long as there is whitespace
    str.erase(str.begin(), std::find_if(str.begin(), str.end(), [](int ch) {
        return !std::isspace(ch);
    }));
	return;
}

void rtrim_str(std::string& str) {
	//erase from left to right as long as there is whitespace
    str.erase(std::find_if(str.rbegin(), str.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), str.end());
	return;
}

void trim_str(std::string& str) {
    ltrim_str(str);
    rtrim_str(str);
	return;
}

std::optional<size_t> parse_uint(const std::string& str) {
	try {
		return static_cast<size_t>(std::stoul(str));
	} catch(std::invalid_argument exc) {
		std::cout << std::endl;
		warning("Warning: \"" + str + "\" is no unsigned integer. Ignoring it...");
	}
	return std::nullopt;
}

std::unordered_set<size_t> parse_uint_seq(const std::string& str, size_t ignore_from_here) {
	std::string buffer;
	return parse_uint_seq(str, buffer, ignore_from_here);
}

std::unordered_set<size_t> parse_uint_seq(const std::string& str, std::string& curr_num_buff, size_t ignore_from_here) {
	std::unordered_set<size_t> uints;
	for (auto i = str.cbegin(); i != str.cend(); i++) {
		//only add the numercial parts
		//else use the previous input to exclude
		if (!is_ASCII_number(*i) && !curr_num_buff.empty()) {
			std::optional<size_t> parsed_uint = parse_uint(curr_num_buff);
			if(parsed_uint.has_value()) {
				if(parsed_uint < ignore_from_here) {
					uints.insert(parsed_uint.value());
				}
			}
			//makes it usable for the next rotation
			curr_num_buff.clear();
		}
		else {
			curr_num_buff += *i; //building number from string
		}
	}
	//in case there is a remainder
	if (!curr_num_buff.empty()) {
		std::optional<size_t> parsed_uint = parse_uint(curr_num_buff);
		if(parsed_uint.has_value()) {
			if(parsed_uint < ignore_from_here) {
				uints.insert(parsed_uint.value());
			}
		}
	}
	return uints;
}

void print_terminal_separator() {
	std::cout << "\n\n---------------------------------------------\n" << std::endl;
	return;
}

size_t random_index(size_t zerotonm1) {
	//the seed shall only be set once
	static bool seed_was_set = false;
	if(!seed_was_set) {
		//setting the random-seed to the current time
		srand(static_cast<unsigned int>(time(0)));
		seed_was_set = true;
	}

	rand(); rand(); rand(); //shift
	return rand() % zerotonm1;
}

bool yes_no_query(std::string& inputbuff, bool reread) {
	if(reread) {
		std::getline(std::cin, inputbuff, '\n');
	}
	trim_str(inputbuff);
	if(!inputbuff.empty()) {
		if(inputbuff.front() == 'y' || inputbuff.front() == 'Y') {
			return true;
		} else if(inputbuff.front() == 'n' || inputbuff.front() == 'N') {
			return false;
		} else {
			//std::cout << "Falsche Eingabe. Neuer Versuch: ";
			std::cout << "Unknown input. Retry: ";
			yes_no_query(inputbuff);
		}
	}
	return false;
}

bool yes_no_request(const std::string& msg) {
	std::cout << msg << " (y/n): ";
	std::string inputbuff;	
	return yes_no_query(inputbuff);
}

