#include "include/arguments.hpp"
#include "main.hpp"

/** @file */

int main(int argc, char* argv[]) {
	//check state of SESSIONS_DIR
	if(!std::filesystem::exists(SESSIONS_DIR)) {
		std::filesystem::create_directory(SESSIONS_DIR);
	} else if(!std::filesystem::is_directory(SESSIONS_DIR)) {
		fatal_error(std::string("Please make sure that ") + SESSIONS_DIR + " is a directory", CODE_INVALID_ARGUMENT);
	}

	//check state of PEOPLE_LISTS_DIR
	if(!std::filesystem::exists(PEOPLE_LISTS_DIR)) {
		std::filesystem::create_directory(PEOPLE_LISTS_DIR);
		fatal_error(MSG_ERROR_NO_LISTS_WERE_FOUND, CODE_NO_LISTS);
	} else if(std::filesystem::is_directory(PEOPLE_LISTS_DIR)) {
		if(std::filesystem::is_empty(PEOPLE_LISTS_DIR)) {
			fatal_error(MSG_ERROR_NO_LISTS_WERE_FOUND, CODE_NO_LISTS);
		}
	} else {
		fatal_error(MSG_ERROR_NO_LISTS_WERE_FOUND, CODE_NO_LISTS);
	}

	arguments args(std::move(std::vector<std::string>(argv, argv+argc)));
	//prepare usage
	//use reference to avoid unique_ptr-copy
	session& sess = args.get_suitable_session();
	//determine the missing people
	sess.determine_missing();

	do {
		
		std::cout << sess.select() << std::endl;

	} while(!query_finished(sess));
	//get the result
	return CODE_EXIT_OK;
}

bool query_finished(session& sess) {
	std::cout << "Aufgabe beendet." << std::endl;
	std::string input;
	std::cout << "Noch einmal? (" << sess.get_n_per_selection() << ") [y/n/neue Anzahl]: ";
	std::getline(std::cin, input, '\n');
	trim_str(input);
	try {
		//if it is no number, assume a y/n input
		sess.set_n_per_selection(std::stoul(input));
		return false;
	} catch(std::exception e) {
		return !yes_no_query(input, false);
	}
	return true;
}

std::ostream& operator<<(std::ostream& os, const selection_result& res) {
	set_color(COLOR_BRIGHT_WHITE);
	os << "\nDienst: " << std::endl;
	set_color(COLOR_AQUA_ON_BRIGHT_WHITE);
	if (res.empty()) {
		os << "--- Leer ---";
		set_color(COLOR_NORMAL);
		std::cout << std::endl;
		return os;
	}
	//listing all names
	size_t i = 0; //index-var
	for (; i < res.size(); i++) {
		set_color(COLOR_AQUA_ON_BRIGHT_WHITE);
		os << i + 1 << "te Instanz:\t" << *res[i];
		set_color(COLOR_NORMAL);
		std::cout << std::endl;
	}
	return os;
}
