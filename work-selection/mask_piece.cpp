#include "include/mask_piece.hpp"

/** @file */

mask_piece::mask_piece()
    : is_in_pool(true), position_in_set() {
    
}

mask_piece::mask_piece(bool is_in_pool, database_t::iterator position_in_set)
    : is_in_pool(is_in_pool), position_in_set(position_in_set) {

}

mask_piece& mask_piece::operator=(const bool is_in_pool) {
    this->is_in_pool = is_in_pool;
    return *this;
}

mask_piece& mask_piece::operator=(const mask_piece& other) {
    this->is_in_pool = other.is_in_pool;
    this->position_in_set = other.position_in_set;
    return *this;
}
mask_piece& mask_piece::operator=(mask_piece&& other) {
    this->is_in_pool = other.is_in_pool;
    this->position_in_set = std::move(other.position_in_set);
    return *this;
}