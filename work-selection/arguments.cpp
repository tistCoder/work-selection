#include "include/arguments.hpp"
#include "include/session.hpp"

/** @file */

#define FLAG_RESET_HOISTED          *i == FLAG_RESET
#define FLAG_SAVE_SETTINGS_HOISTED  *i == FLAG_SAVE_SETTINGS
#define ANY_ARG_N_TO_CHOOSE         (*i == ARG_N_TO_CHOOSE) || (*i == ARG_N_TO_CHOOSE_LONG)
#define ANY_ARG_SESSION_NAME        (*i == ARG_SESSION_NAME) || (*i == ARG_SESSION_NAME_LONG)

void arguments::evaluate_args() {
    std::vector<std::string>::iterator pos_arg_session_name;
    people_list people;

    //skip argument 1 as it is the path to this executable
    for(auto i = (this->_args.begin()+1); i != this->_args.end(); i++) {
        //if a reset was requested
        if(FLAG_RESET_HOISTED) {
            this->_flag_reset.hoist();
        }
        //if the settings are supposed to be saved
        else if(FLAG_SAVE_SETTINGS_HOISTED) {
            this->_flag_save_settings.hoist();
        }
        //if there is a specific number of people to select
        else if(ANY_ARG_N_TO_CHOOSE) {
            i++;
            if(this->_n_to_choose.was_set()) {
                fatal_error(MSG_ERROR_REPEATED_ARGUMENT(ARG_N_TO_CHOOSE_LONG), CODE_REPEATED_ARG);
            }
            if(i != this->_args.end()) {
                if(is_ASCII_number(*i)) {
                    this->_n_to_choose = static_cast<size_t>(std::stoul(*i));
                } else {
                    fatal_error(MSG_ERROR_INVALID_NUMBER(*i), CODE_INVALID_NUMBER);
                }
            } else {
                fatal_error(MSG_ERROR_MISSING_ARG_SPECIFIER(*(i-1)), CODE_MISSING_ARG_SPECIFIER);
            }
        }
        //if there is a specific session to use / create
        else if(ANY_ARG_SESSION_NAME) {
            if(this->_session_name.was_set()) {
                fatal_error(MSG_ERROR_REPEATED_ARGUMENT(ARG_SESSION_NAME_LONG), CODE_REPEATED_ARG);
            }
            i++;
            if(i == this->_args.end()) {
                //TODO
                throw 10;
            }
            pos_arg_session_name = i;
            if(!has_suffix(*i, SESSION_EXTENSION)) {
                *i += SESSION_EXTENSION;
            }
            this->_session_name.set_value(*i);
        } else {
            if(pos_arg_session_name == i-1) {
                if(!has_suffix(*i, LIST_EXTENION)) {
                    *i += LIST_EXTENION;
                }
                people.load(*i);
                continue;
            }
            fatal_error(MSG_ERROR_INVALID_ARGUMENT(*i), CODE_INVALID_ARGUMENT);
        }
    }
    //setting up the suitable session
    this->_session.ini_fresh(this->_session_name.value(), std::move(people));
    this->_session.set_n_per_selection(this->_n_to_choose.value());
    if(this->_flag_reset.state()) {
        this->_session.reset();
    }
    return;
}

arguments::arguments(std::vector<std::string>&& args)
    : _args(std::move(args)) {
    this->evaluate_args();
}

bool arguments::flag_state_reset() const {
    return this->_flag_reset.state();
}

size_t arguments::n_to_choose() const {
    return this->_n_to_choose.value();
}

const std::string& arguments::session_name() const {
    return this->_session_name.value();
}

session& arguments::get_suitable_session() {
    return this->_session;
}