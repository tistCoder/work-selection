#include <iostream>
#include <filesystem>
#include <fstream>
#include <string>

/** @file */

#include "include/tools.hpp"
#include "include/people_list.hpp"

void print_list(const std::string& list_path);

void edit_list(const std::string& list_path);

void create_list(const std::string& list_path);

//no args
int main() {

	//in case there is no "lists"-folder
    if(!std::filesystem::exists(PEOPLE_LISTS_DIR)) {
		//create the missing directory
		std::filesystem::create_directory(PEOPLE_LISTS_DIR);
	}
	//in case of existance but as a regular file
	else if(!std::filesystem::is_directory(PEOPLE_LISTS_DIR)) {
		fatal_error(MSG_ERROR_NO_LISTS_WERE_FOUND, CODE_NO_LISTS);
	}

    //std::cout << "Willkommen zur Verwaltung von Personengruppen (Listen)." << std::endl
	//			<< "Drücken Sie Ctrl+C, um das Programm zu beenden." << std::endl;
	std::cout	<< "Welcome to the administration of groups of people (lists)." << std::endl
				<< "Press Ctrl+C in order to terminate this program." << std::endl;
    do {
		//std::cout << "Bitte geben Sie den Namen der gewünschten Liste ein: ";
		std::cout << "Please type the demanded list's name: ";
		//name of the list
    	std::string list_name = "";
    	std::cin >> list_name;
		
		if(!has_suffix(list_name, LIST_EXTENION)) {
			list_name += LIST_EXTENION;
		}
		list_name = PEOPLE_LISTS_DIR + list_name;

		//on potential edit
		if(std::filesystem::exists(list_name)) {
			if(yes_no_request("Die angegebene Liste (" + list_name + ") existiert bereits. Wollen Sie diese bearbeiten?")) {
				
			}
		}
		//on potential create
		else {
			if(yes_no_request("Die angegebene Liste (" + list_name + ") existiert nicht. Wollen Sie diese erstellen und definieren?")) {
				create_list(list_name);
			}
		}
		std::cout << std::endl << std::endl;
	} while(true);
    return CODE_EXIT_OK;
}

void print_list(const std::string& list_path) {
	
	return;
}

void edit_list(const std::string& list_path) {

	return;
}

void create_list(const std::string& list_path) {

	return;
}