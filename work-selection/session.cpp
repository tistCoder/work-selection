#include "include/session.hpp"

/** @file */

session::session()
	: _people(), _requires_loading(true) {
	
}

session::session(const std::string& name, size_t n_per_selection)
        : _file_path(SESSIONS_DIR + name), _n_per_selection(n_per_selection) {
    this->load(this->_file_path);
}

void session::ini_fresh(const std::string& name, people_list&& list) {
	this->_file_path = SESSIONS_DIR + name;
	this->_people = std::move(list);
	if(!list.loaded()) {
		this->load(this->_file_path);
		return;
	}
	this->ini_mask();
	this->_pool.resize(this->_mask->size());

	size_t pos = 0;
	for(auto i = this->_people.cbegin(); i != this->_people.cend(); i++, pos++) {
		this->_mask->at(pos) = mask_piece(true, i);
		this->_pool.at(pos) = this->_mask->begin()+pos;
	}
	this->define_user_relevant_rpool_upper_bound();
	return;
}

void session::ini_mask() {
	//in order to avoid reallocation
	//dynamically allocate when the exact size is known
    this->_mask = std::unique_ptr<mask_t>(new mask_t(this->_people.size()));
	
	{
		auto mask_pos = this->_mask->begin();
		for(auto i = this->_people.cbegin(); i != this->_people.cend(); i++, mask_pos++) {
			*mask_pos = mask_piece(true, i);
		}
	}
	return;
}

void session::define_user_relevant_rpool_upper_bound() {
	//growth-behavior: constant or growing
	//when growing: new elements are guaranteed to be available
	//and therefore need not to be inspected again
	this->_pos_in_rpool_to_ask_to = this->_reserve_pool.size();
	return;
}

void session::compensate_insufficient_set() {
	std::string determined, curr_num_buff;
	std::unordered_set<size_t> indices;
	this->compensate_insufficient_set(determined, curr_num_buff, indices);
	return;
}

void session::compensate_insufficient_set(std::string& determined, std::string& curr_num_buff, std::unordered_set<size_t>& indices) {

	if (this->_reserve_pool.empty()) {
		fatal_error(MSG_ERROR_TOO_FEW_ELEMS, CODE_TOO_FEW_ELEMS);
	} else if ((this->_pool.size() + this->_reserve_pool.size()) < this->_n_per_selection) {
		fatal_error(MSG_ERROR_TOO_FEW_ELEMS, CODE_TOO_FEW_ELEMS);
	}

	if(this->_ask_for_rpools_missing && (this->_pos_in_rpool_to_ask_to > 0)) {
		print_terminal_separator();
		//UI
		this->print_reserve_pool(this->_pos_in_rpool_to_ask_to);
		/*std::cout	<< "\nAufgrund der Insuffizienz jener Menge werden bereits selektierte hinzugezogen."
					<< "\nBitte wiederholen Sie die Prozedur mit jener gegebenen Menge:"
					<< std::endl;*/
		std::cout	<< "\nDue to the insufficience of the given set, previously selected people are now being taken into consideration."
					<< "\nPlease repeat the procedure with the new set:"
					<< std::endl;
		//std::cout << "Eingabe: ";
		std::cout << "Input: ";
		std::getline(std::cin, determined, '\n');
		trim_str(determined);
		if (!determined.empty()) {
			curr_num_buff.clear(); //make it usable again
			//no more alternatives
			switch (determined[0]) {
			case 'n':
				fatal_error(MSG_ERROR_TOO_FEW_ELEMS, CODE_TOO_FEW_ELEMS);
			case 'e':
				exit(CODE_EXIT_OK);
			case 'r':
				this->reset();
				this->save();
				this->determine_missing();
				return;
			default:
				//in case of a change
				indices = parse_uint_seq(determined, curr_num_buff, this->_pos_in_rpool_to_ask_to);
				remove_aggregated_tail_in_place(this->_reserve_pool, indices);
				break;
			}
		}
		else {
			std::cout << MSG_NOTHING_SPARED << std::endl;
		}
		this->_ask_for_rpools_missing = false;
	}
	merge_append_in_place(this->_pool, this->_reserve_pool);
	return;
}

void session::save() const {
	std::ofstream pool_writer(this->_file_path, std::ios::trunc);
	if(!pool_writer.is_open()) {
		return; //TODO: exception
	}
	pool_writer << '"' << this->_people.name() << "\" " << this->_people.hash_value() << std::endl;
	
	for(auto i = this->_mask->cbegin(); i != (this->_mask->cend()-1); i++) {
		pool_writer << i->is_in_pool;
	}
	pool_writer << (this->_mask->cend()-1)->is_in_pool;
	pool_writer.close();
	return;
}

void session::reset() {
	this->_pool.clear();
	this->_reserve_pool.clear();
	this->_requires_loading = false;

	//add all mask_pieces to pool
	for(auto i = this->_mask->begin(); i != this->_mask->end(); i++) {
		this->_pool.push_back(i);
	}
	//set all mask_pieces to 'is in pool' = true
	std::fill(this->_mask->begin(), this->_mask->end(), true);
	if(this->_pool.empty()) {
		fatal_error(MSG_ERROR_NONSENSE_BASE_LIST, CODE_NONSENSE_BASE_LIST);
	}
	this->define_user_relevant_rpool_upper_bound();
	return;
}

void session::print_pool() const {
	//<index>:	<name at position in set>
	for(size_t i = 0; i < this->_pool.size(); i++) {
		std::cout << i << ":\t" << *this->_pool.at(i)->position_in_set << std::endl;
	}
	return;
}

void session::print_reserve_pool() const {
	//print entire reserve pool
	this->print_reserve_pool(std::string::npos);
	return;
}

void session::print_reserve_pool(size_t to) const {
	//translating coded end to end of reserve pool
	if(to == std::string::npos) {
		to = this->_reserve_pool.size()-1;
	} else if(to >= this->_reserve_pool.size()) {
		to = this->_reserve_pool.size()-1;
	}
	//<index>:	<name at position in set>
	for(size_t i = 0; i <= to; i++) {
		std::cout << i << ":\t" << *this->_reserve_pool.at(i)->position_in_set << std::endl;
	}
	return;
}

const people_list& session::get_people_list() const {
	return this->_people;
}

bool session::requires_loading() const {
	return this->_requires_loading;
}

void session::load(const std::string& filepath) {
	std::ifstream file(filepath);
	std::string input;

	if (!file.is_open()) {
    	//reset if the file does not exist
    	if (!file.good()) {
    	    this->reset();
	    }
        else {
	        fatal_error(MSG_ERROR_UNABLE_TO_LOAD_FILE(this->_file_path), CODE_OPEN_FAILURE);
	    }
    }
	this->_requires_loading = false;
	//reading headline
	std::getline(file, input, '\n');
    if(!this->_people.load_and_check(input)) {
		std::cout << "The parent-list was modified and therefore cannot reliably bind to this session." << std::endl;
        if(yes_no_request("Do you wish to reset and continue? Program will exit otherwise")) {
			this->ini_mask();
            this->reset();
			return;
        } else {
			exit(CODE_EXIT_OK);
		}
	}
    this->ini_mask();
	//loading actual save (sequence of 0|1)
	std::getline(file, input, '\n');
	//adding names to the pool
	for (size_t i = 0; i < input.size(); i++) {
		//0 translates to false, else true
		bool is_in_pool = input[i] != '0';
		this->_mask->at(i).is_in_pool = is_in_pool;

		//creating souvereign nodes
		auto pos = this->_mask->begin();
		//translating counter to corresponding iterator
		pos += i;
		//categorizing all mask-elements
		if (is_in_pool) {
			this->_pool.push_back(pos);
		}
		else {
			this->_reserve_pool.push_back(pos);
		}
	}
	file.close();
	//reset in case, it is empty
	if (this->_pool.empty()) {
		this->reset();
	}
	this->define_user_relevant_rpool_upper_bound();
	return;
}

void session::determine_missing() {
	//UI
	//std::cout << "Liste alle passiv-auswählbaren Personen auf:\n" << std::endl;
	std::cout << "\nListing all passively selectable people:\n" << std::endl;
	set_color(COLOR_BLACK_ON_BRIGHT_WHITE);
	//std::cout << "Nummer\tPerson";
	std::cout << "Number\tPerson";
	set_color(COLOR_NORMAL);
	std::cout << std::endl;
	//listing all who have not been selected before
	this->print_pool();
	std::cout	<< "\nWrite down the number of each missing person separated by space." << std::endl
				<< "A reset, if necessary, can be forced by writing 'r' only." << std::endl
				<< "In case nobody is present, write 'n' only in order to exclude them all." << std::endl
				<< "If you want to exit without saving changes concerning this iteration, type 'e' only." << std::endl;
	/*std::cout	<< "\nSchreiben Sie alle fehlenden Personen leerzeichensepariert als Nummer auf." << std::endl
				<< "Ein Reset, sofern gewollt, kann mit 'r' forciert werden." << std::endl
				<< "Sollte keiner der hier aufgeführten Personen vor Ort sein, so geben Sie 'n' ein:" << std::endl;*/
	if(this->_n_per_selection != 1) {
		//std::cout << "Es werden hiernach " << this->_n_per_selection << " Personen ausgewählt." << std::endl;
		std::cout << "Afterwards " << this->_n_per_selection << " people will be selected." << std::endl;
	} else {
		//std::cout << "Es wird hiernach eine Person ausgewählt." << std::endl;
		std::cout << "Afterwards one person will be selected." << std::endl;
	}

	//std::cout << "Eingabe: ";
	std::cout << "Input: ";
	std::string determined;		//the typed numbers
	std::string curr_num_buff; 	//in order to minimize reallocation
	//the translated numbers (no repetitions possible)
	//getting the ids of the missing people
	std::getline(std::cin, determined, '\n');
	trim_str(determined);
	std::unordered_set<size_t> indices;

	//in case there was any change
	if (!determined.empty()) {
		//checking for "none", "reset" or "exit"
		switch (determined[0]) {
		case 'n':
			this->_pool.clear();
			break;
		case 'r':
			this->reset();
			this->save();
			this->determine_missing();
			return;
		case 'e':
			exit(CODE_EXIT_OK);
		default:
			//in case of a change
			indices = parse_uint_seq(determined, curr_num_buff);
			remove_aggregated_tail_in_place(this->_pool, indices);
			break;
		}
	} else {
		std::cout << MSG_NOTHING_SPARED << std::endl;
	}
	if ((this->_pool.size() < this->_n_per_selection)) {
		this->compensate_insufficient_set(determined, curr_num_buff, indices);
	}
	return;
}

selection_result session::select() {
	selection_result result(this->_n_per_selection);
	size_t n_left_to_select = this->_n_per_selection;
	size_t pool_size = 0;
	if(this->_pool.size() < this->_n_per_selection) {
		pool_size = this->_pool.size();
		n_left_to_select -= this->_pool.size();
		this->compensate_insufficient_set();
		//excluding the ones who are left and therefore have to be selected
		for(size_t i = 0; i < pool_size; i++) {
			auto& selection = this->_pool.at(i);
			selection->is_in_pool = false;
			result[i] = std::addressof(*selection->position_in_set);
			std::swap(selection, this->_pool.at(this->_pool.size()-i-1));
		}
	}
	for(size_t i = 0; i < n_left_to_select; i++) {
		//random index in range of pool
		size_t index = random_index(this->_pool.size()-i-pool_size);
		//access selected element
		auto& selection = this->_pool.at(index);
		//make it unavailable in mask for saving purposes
		selection->is_in_pool = false;
		//add pointer to element to the result
		result[pool_size+i] = std::addressof(*selection->position_in_set);
		//aggregate tail for resizing
		std::swap(selection, this->_pool.at(this->_pool.size()-i-1));
	}
	//move tail to reserve_pool
	for(size_t i = 0; i < n_left_to_select; i++) {
		this->_reserve_pool.push_back(this->_pool.back());
		this->_pool.pop_back();
	}
	//saving progress after every selection
	this->save();
	return result;
}

size_t session::get_n_per_selection() const {
	return this->_n_per_selection;
}

void session::set_n_per_selection(size_t  n_per_selection) {
	this->_n_per_selection = n_per_selection;
	return;
}

session::~session() {

}