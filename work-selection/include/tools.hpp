#pragma once

/** @file */

#include "mask_piece.hpp"

#include <algorithm>
#include <cctype> 
#include <iostream>
#include <locale>
#include <optional>
#include <string>
#include <unordered_set>

//for colored outputs
#ifdef WIN32
	#include <Windows.h>
#endif

/** The type of a constant name (compile-time and immutable string) **/
#define CONSTANT_NAME static constexpr const char*

/** file name: settings.cfg **/
CONSTANT_NAME SETTINGS_FILE					=	"settings.cfg";

/* Error Codes */

/** Code: Execution was successful **/
constexpr int CODE_EXIT_OK = 0;
/** Code: Could not open a mandatory file **/
constexpr int CODE_OPEN_FAILURE = 1;
/** Code: Could not reset the selection-list **/
constexpr int CODE_RESET_FAILURE = 2;
/** Code: Too few elements for selection **/
constexpr int CODE_TOO_FEW_ELEMS = 3;
/** Code: A string did not represent a number **/
constexpr int CODE_INVALID_NUMBER = 4;
/** Code: A program argument (parameter of main()) was invalid **/
constexpr int CODE_INVALID_ARGUMENT = 5;
/** Code: A program argument was not specified **/
constexpr int CODE_MISSING_ARG_SPECIFIER = 6;
/** Code: A program argument was named multiple times **/
constexpr int CODE_REPEATED_ARG = 7;
/** Code: If directory "./lists" is either empty or not existing **/
constexpr int CODE_NO_LISTS = 8;
/** Code: If the reset of a sessions produced an empty pool **/
constexpr int CODE_NONSENSE_BASE_LIST = 9;


/* (Error) Messages */
/** Message: No one was excluded from a selection-pool **/
#define MSG_NOTHING_SPARED						std::string("\nKeine Ausnahmen entfernt")
/** Message-Warning: An invalid index was not taken into consideration due to a syntax error **/
#define MSG_WARNING_INVALID_ID(id)				std::string("\nWarnung: Die angegebene ID ") + std::to_string(id) \
												+ " ist invalide - sie wurde vom Extraktionsprozess ausgeschlossen!"
/** Message-Error: Too few elements for selection **/
#define MSG_ERROR_TOO_FEW_ELEMS					std::string("Too few people for the demanded amount!")
/** Message-Error:  **/
#define MSG_ERROR_UNREACHED_MIN					std::string("The minimum was not set properly!")
/** Message-Error:  **/
#define MSG_ERROR_UNABLE_TO_LOAD_FILE(filename) std::string("Could not open \"") + filename + "\"!"
/** Message-Error:  **/
#define MSG_ERROR_BAD_BACKUP_FILE(filename)		std::string("The given backupfile \"") + filename + "\" was inconclusive!"
/** Message-Error:  **/
#define MSG_ERROR_INVALID_NUMBER(number)		std::string("The given number \"") + number + "\" is malformed!"
/** Message-Error:  **/
#define MSG_ERROR_INVALID_ARGUMENT(arg)			std::string("The given argument \"") + arg + "\" is invalid!"
/** Message-Error:  **/
#define MSG_ERROR_MISSING_ARG_SPECIFIER(arg_missing_specifier) std::string("The given argument \"") + arg_missing_specifier + "\" requires a specifier"
/** Message-Error:  **/
#define MSG_ERROR_REPEATED_ARGUMENT(which)		std::string("The given argument \"") + which + "\" was already specified!"
/** Message-Error:  **/
#define MSG_ERROR_NO_LISTS_WERE_FOUND			std::string("No list for any selection was found!")
/** Message-Error: **/
#define MSG_ERROR_NONSENSE_BASE_LIST			std::string("The list of this session is malformed!")

/* Color Codes */

#ifdef WIN32
	#define COLOR_YELLOW			0x06
	#define COLOR_LIGHT_RED 		0x0C
	#define COLOR_RED				0x04
	#define COLOR_NORMAL 			0x07
	#define COLOR_BRIGHT_WHITE	    0x0F
	#define COLOR_BLACK_ON_BRIGHT_WHITE	0xF0
	#define COLOR_AQUA_ON_BRIGHT_WHITE	0xF3
#else
	#define COLOR_YELLOW					"\033[1;33m"
	#define COLOR_LIGHT_RED 				"\033[1;31m"
	#define COLOR_RED						"\033[0;31m"
	#define COLOR_NORMAL 					"\033[0;0m"
	#define COLOR_BRIGHT_WHITE				"\033[1;37m"
	#define COLOR_BLACK_ON_BRIGHT_WHITE		"\033[0;30;47m"
	#define COLOR_AQUA_ON_BRIGHT_WHITE		"\033[0;34;47m"
#endif

/**
 * Prints an error message and exits the program with
 * the given exit code
 * 
 * @param msg error message
 * @param exit_code an exit code for the exit of this program
 */
DYNLIB_EXPORT void fatal_error(const std::string& msg, int exit_code);

/**
 * Checks whether a char represents a number (latin only)
 * 
 * @param c char being a number potentially
 */
DYNLIB_EXPORT bool is_ASCII_number(char c);

/**
 * Checks whether a std::string represents a number (latin only)
 * 
 * @param str string being a number potentially
 */
DYNLIB_EXPORT bool is_ASCII_number(const std::string& str);

#ifdef WIN32
	/**
	 * Sets the output color to an 8-bit-color
	 * using the Windows-API
	 * 
	 * @param color 8-bit-color-code according to <a href="https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/color#remarks">Microsoft's remarks</a>
     * @param os unused as it is only for compatibility
	 */
    DYNLIB_EXPORT void set_color(uint8_t color, std::ostream& os = std::cout);

	/**
	 * Prints colored text
	 * 
	 * @param color color of the text
	 */
    DYNLIB_EXPORT void printc(const std::string& msg, uint8_t color);

	/**
	 * Prints colored text and appends a new line
	 * 
	 * @param color colro of the text
	 */
    DYNLIB_EXPORT void printlnc(const std::string& msg, uint8_t color);

#else
	/**
	 * Sets the output color to an 8-bit-color using ANSI-escape-characters whilst
	 * allowing to change the output stream
	 * 
	 * @param ansi_escape_char an ANSI-escape-char according to <a href="https://en.wikipedia.org/wiki/ANSI_escape_code"> ANSI escpae code on Wikipedia</a>
	 * @param os output stream
	 */
    DYNLIB_EXPORT void set_color(const char* ansi_escape_char, std::ostream& os = std::cout);

	/**
	 * Prints colored text
	 * 
	 * @param ansi_escape_char color of the text
	 */
    DYNLIB_EXPORT void printc(const std::string& msg, const char* ansi_escape_char);

	/**
	 * Prints colored text and appends a new line
	 * 
	 * @param ansi_escape_char color of the text
	 */
    DYNLIB_EXPORT void printlnc(const std::string& msg, const char* ansi_escape_char);

#endif

/**
 * Prints a warning
 * 
 * @param msg warning message
 */
DYNLIB_EXPORT void warning(const std::string& msg);

/**
 * Checks whether a string has a certain prefix
 * 
 * @param str string to analyze
 * @param prefix prefix to check for
 */
DYNLIB_EXPORT bool has_prefix(const std::string& str, const std::string& prefix);

/**
 * Checks whether a string has a certain suffix
 * 
 * @param str string to analyze
 * @param suffix suffix to check for
 */
DYNLIB_EXPORT bool has_suffix(const std::string& str, const std::string& suffix);

/**
 * Checks whether a string has a certain suffix
 * and replaces it with another one
 * 
 * @param str string to analyze
 * @param suffix suffix to check for
 * @param new_suffix suffix to replace the old one
 */
DYNLIB_EXPORT bool has_suffix_and_replace_if(std::string& str, const std::string& suffix, const std::string& new_suffix);

/**
 * Checks whether a string has a certain suffix
 * and replaces it with another one
 * 
 * @param str string to analyze
 * @param suffix suffix to check for
 * @param new_suffix suffix to replace the old one
 */
DYNLIB_EXPORT bool has_suffix_and_replace_if(std::string& str, const std::string& suffix, std::string&& new_suffix);

/**
 * Replaces the suffix beginning from start with a new one
 * [and appends its overflowing characters if nescessary] in place
 * 
 * @param str string whose suffix is meant to be replaced
 * @param start the start of the old suffix
 * @param new_suffix the new suffix
 */
DYNLIB_EXPORT void replace_suffix_in_place(std::string& str, size_t start, const std::string& new_suffix);

/**
 * Replaces the suffix beginning from start with a new one
 * [and appends its overflowing characters if nescessary] in place
 * 
 * @param str string whose suffix is meant to be replaced
 * @param start the start of the old suffix
 * @param new_suffix the new suffix
 */
DYNLIB_EXPORT void replace_suffix_in_place(std::string& str, size_t start, std::string&& new_suffix);

/**
 * Removes all whitespace before the first non-whitespace letter appears
 * (left-to-right, in-place operation)
 * 
 * @param str string to trim from the left side
 */
DYNLIB_EXPORT void ltrim_str(std::string& str);

/**
 * Removes all whitespace before the first non-whitespace letter appears
 * (right-to-left, in-place operation)
 * 
 * @param str string to trim from the left side
 */
DYNLIB_EXPORT void rtrim_str(std::string& str);

/**
 * Removes all whitespace before the first non-whitespace letter appears
 * (left-to-right + right-to-left, in-place operation)
 * 
 * @param str string to trim from the left side
 */
DYNLIB_EXPORT void trim_str(std::string& str);

DYNLIB_EXPORT std::optional<size_t> parse_uint(const std::string& str);

/**
 * Extracts unsigned integers from a string
 * where each number is separated by space
 * 
 * @param str string to read sequence from
 * @param ignore_from_here omit any parsed uint greater-equals this upper bound (std::string::npos omit nothing)
 */
DYNLIB_EXPORT std::unordered_set<size_t> parse_uint_seq(const std::string& str, size_t ignore_from_here = std::string::npos);

/**
 * Extracts unsigned integers from a string
 * where each number is separated by space
 * 
 * @param str string to read sequence from
 * @param curr_num_buff an existing buffer for extracting the number
 * @param ignore_from_here omit any parsed uint greater-equals this upper bound (std::string::npos omit nothing)
 */
DYNLIB_EXPORT std::unordered_set<size_t> parse_uint_seq(const std::string& str, std::string& curr_num_buff, size_t ignore_from_here = std::string::npos);

/**
 * Moves chosen elements to the back of a given vector
 * by swapping these with the old tail's elements (tail-aggregation).
 * And removes that very tail by resizing the vector (removal)
 * 
 * @param vec vector to aggregate and remove a tail in
 * @param indices set of indices whose elements aggregate the tail
 */
template<typename T>
void remove_aggregated_tail_in_place(std::vector<T>& vec, const std::unordered_set<size_t>& indices) {
	size_t new_size = vec.size()-1;

	for(auto i = indices.cbegin(); i != indices.cend(); i++) {
		auto dest = vec.begin() + new_size;
		std::swap(*(vec.begin() + *i), *dest);
		new_size--;
	}
	new_size++;
	vec.resize(new_size);
}

/**
 * Merges two vectors into a single one by moving from's elements
 * into 'into'
 * 
 * @param into vector to move the elements of 'from' into
 * @param from vector to move the elements from
 */
template<typename T>
void merge_append_in_place(std::vector<T>& into, std::vector<T>& from) {
	auto begin = into.end()-1;
	size_t old_size = into.size();
	into.resize(from.size() + old_size);
	for(size_t i = 0; i < from.size(); i++, old_size++) {
		into[old_size] = std::move(from[i]);
	}
	from.clear();
	return;
}

/**
 * Prints a terminal separator
 * consisting of hyphens and newlines
 * 
 */
DYNLIB_EXPORT void print_terminal_separator();

/**
 * Generates a random natural number in [0;zerotonm1)
 * 
 * @param zerotonm1 reads "from zero to n-1"
 */
DYNLIB_EXPORT size_t random_index(size_t zerotonm1);

/**
 * Checks if the user typed y/Y|n/N and translates
 * it to a boolean-expression
 * 
 * @param inputbuff buffer to [use and] evaluate
 * @param reread determines whether the buffer needs to be updated
 */
DYNLIB_EXPORT bool yes_no_query(std::string& inputbuff, bool reread = true);

/**
 * Prints a given request and evaluates it as
 * a yes_no_query(std::string& inputbuff, bool reread)
 * 
 * @param msg message conveying the request
 */
DYNLIB_EXPORT bool yes_no_request(const std::string& msg);