#pragma once

/** @file */

#include "argument.hpp"
#include "flag.hpp"
#include "main.hpp"
#include "session.hpp"

#include <iostream>
#include <string>
#include <vector>

#define ARGUMENT_NAME_TYPE static constexpr const char*

/**
 * Reinterprets argv of main(argc : int, argv : char*[]) as
 * more abstract objects in order to handle the demanded options
 * 
 * @brief An abstraction layer to make datatypes behave like program arguments
 */
class DYNLIB_EXPORT arguments {
public:

    /**
     * Use: -c \<unsigned integer\>
     */
    ARGUMENT_NAME_TYPE ARG_N_TO_CHOOSE          = "-c";

    /**
     * Use: -choose <\unsigned integer\>
     */
    ARGUMENT_NAME_TYPE ARG_N_TO_CHOOSE_LONG     = "-choose";

    /**
     * Use: -s \<session name [on load]\> (\<parent list [on create]\>)
     */
    ARGUMENT_NAME_TYPE ARG_SESSION_NAME         = "-s";

    /**
     * Use: -session \<session name [on load]\> (\<parent list [on create]\>)
     */
    ARGUMENT_NAME_TYPE ARG_SESSION_NAME_LONG    = "-session";

    ARGUMENT_NAME_TYPE FLAG_SAVE_SETTINGS       = "--save-settings";
    ARGUMENT_NAME_TYPE FLAG_RESET               = "--reset";
    ARGUMENT_NAME_TYPE FLAG_HELP                = "--help";

private:
    std::vector<std::string> _args;
    session _session;

    flag _flag_reset;
    flag _flag_save_settings;
    argument<size_t> _n_to_choose           = 1;
    argument<std::string> _session_name     = argument<std::string>("");

    void evaluate_args();

public:

    /**
     * Uses a higher-level dynamic array for storing
     * the passed program-arguments and interprets them
     * 
     * @param args arguments to interpret
     */
    explicit arguments(std::vector<std::string>&& args);

    /**
     * @return 'true' when FLAG_RESET was hoisted
     */ 
    bool flag_state_reset() const;

    /**
     * @return current amount of people to select per iteration
     */
    size_t n_to_choose() const;

    /**
     * @return the name of the constructed session
     */
    const std::string& session_name() const;

    /**
     * Provides access to a session
     * fulfilling the demands of the passed arguments
     * 
     * @return reference to the session
     */
    session& get_suitable_session();

};