#pragma once

/** @file */

#include <algorithm>
#include <memory>

#include "people_list.hpp"

class arguments;

/** file extension: .session **/
CONSTANT_NAME SESSION_EXTENSION				=	".session";

/** directory name: sessions **/
CONSTANT_NAME SESSIONS_DIR 					=	"sessions/";

/**
 * Masks a chosen people_list
 * so its elements can be filtered
 * and thereby used for the selection
 * process
 */
class DYNLIB_EXPORT session {
private:
    people_list _people;
	std::string _file_path;

	bool _requires_loading = true;
	bool _ask_for_rpools_missing = true;
	size_t _pos_in_rpool_to_ask_to = std::string::npos;

    //!the people to choose from
	pool_t _pool;
	//!previously chosen people
	pool_t _reserve_pool;
	std::unique_ptr<mask_t> _mask = nullptr;
	size_t _n_per_selection;

	/**
	 * Initializes the mask as it is allocated dynamically
	 * and thus requires special treatment
	 */
	void ini_mask();
	
	/**
	 * Defines the upper bound of this reserve_pool,
	 * so only non-trivial attendencies are supervised by the user
	 */
	void define_user_relevant_rpool_upper_bound();

	/**
	 * Called whenever the reserve_pool could
	 * potentially yield enough people to reach
	 * the desired amount of people to select.
	 * 
	 * This overload automatically allocates
	 * nescessary memory for its task
	 */
	void compensate_insufficient_set();

	/**
	 * Called whenever the reserve_pool could
	 * potentially yield enough people to reach
	 * the desired amount of people to select.
	 * 
	 * This overload allows the passing of memory
	 * which is overridden
	 * 
	 * @param determined pre-allocated user-input-buffer
	 * @param curr_num_buff pre-allocated number-building-buffer
	 * @param indices pre-allocated set for indices
	 */
	void compensate_insufficient_set(std::string& determined, std::string& curr_num_buff, std::unordered_set<size_t>& indices);

public:	
	
	session();

    session(const std::string& name, size_t n_per_selection);

	/**
	 * Used to initialize a new session from the name
	 * and a pre-loaded list
	 * 
	 * @param name this session's name
	 * @param list pre-loaded people_list
	 */
	void ini_fresh(const std::string& name, people_list&& list);

	/**
	 * Saves this session to the corresponding file
	 */
	void save() const;

	/**
	 * Moves all people to the pool
	 * as if the session was recently
	 * created
	 */
	void reset();

	/**
	 * Prints the entire pool to the screen
	 */
	void print_pool() const;

	/**
	 * Prints the entire reserve_pool to the screen
	 */
	void print_reserve_pool() const;

	/**
	 * Prints the reserve_pool's elements
	 * zero to 'to' to the screen
	 */
	void print_reserve_pool(size_t to) const;

	/**
	 * @return reading-access to the list of this
	 * session
	 */
	const people_list& get_people_list() const;

	/**
	 * @return tells whether this session
	 * still needs to be loaded
	 */
	bool requires_loading() const;

	/**
	 * Loads this session from a certain file
	 * 
	 * @param filepath path to this session
	 */
	void load(const std::string& filepath);

	/**
	 * Asks the user to filter out currently
	 * missing people
	 */
	void determine_missing();

	/**
	 * Randomly selects n_per_selection people
	 * from the pool
	 * 
	 * @return vector of pointers to the selected people
	 */
	selection_result select();
	
	void set_n_per_selection(size_t n_per_selection);

	size_t get_n_per_selection() const;

	virtual ~session();
};