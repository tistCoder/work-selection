#pragma once

/** @file */

#include <set>
#include <string>
#include <vector>

#include "work_selection.hpp"

typedef std::set<std::string>   		database_t;
typedef std::vector<const std::string*> selection_result;

/**
 * Element in mask_t
 */
struct DYNLIB_EXPORT mask_piece {
    //!used for saving/loading: 'true' on not selected before
    bool is_in_pool;
    //!multi-functional reference to element in the database
    database_t::iterator position_in_set;

    /**
     * Shortcut overload for STL-algorithms (e.g. std::fill)
     * 
     * @param is_in_pool value for mask_piece::is_in_pool
     */
    mask_piece& operator=(const bool is_in_pool);

    mask_piece& operator=(const mask_piece& other);
    mask_piece& operator=(mask_piece&& other);

    explicit mask_piece();
    explicit mask_piece(bool is_in_pool, database_t::iterator position_in_set);
};

typedef std::vector<mask_piece>         mask_t;
typedef std::vector<mask_t::iterator>   pool_t;