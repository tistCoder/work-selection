#pragma once

/** @file */

#include <iostream>
#include "work_selection.hpp"

/**
 * Wrapps a type and restricts it to program-argument use
 * 
 * @tparam T type this argument has to wrap
 */
template<class T>
struct argument {
private:
    T _value;
    bool _was_set = false;

public:

    argument(T&& value)
        : _value(std::forward<T>(value)) {
    }

    argument<T>& operator=(T&& val) {
        this->set_value(std::forward<T>(val));
        return *this;
    }

    void set_value(const T& val) {
        this->_value = val;
        this->_was_set = true;
        return;
    }

    void set_value(T&& val) {
        this->_value = std::move(val);
        this->_was_set = true;
        return;
    }

    const T& value() const {
        return this->_value;
    }

    bool was_set() const {
        return this->_was_set;
    }

};