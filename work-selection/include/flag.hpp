#pragma once

/** @file */

#include <iostream>

#include "work_selection.hpp"

/**
 * This encapsulated boolean is set to 'false' initially
 * and can only be hoisted (set to 'true') afterwards
 */
struct DYNLIB_EXPORT flag {
private:
    bool _state;

public:

    /**
     * Creates an flag being not hoisted
     */
    flag();

    /**
     * Sets the internal boolean to 'true'
     */
    void hoist();

    /**
     * Proivides the current state of this flag
     */
    bool state() const;
};