#pragma

/** @file */

#include <fstream>

#include <botan/hash.h>
#include <botan/hex.h>

#include "tools.hpp"

#define CHECKSUM_TYPE "SHA-3(512)"

/** file extension: .list **/
CONSTANT_NAME LIST_EXTENION					=	".list";

/** directory name: lists **/
CONSTANT_NAME PEOPLE_LISTS_DIR				= 	"lists/";

/**
 * Entire list of people for
 * a session to handle
 */
class DYNLIB_EXPORT people_list {
private:
    bool _loaded = false;
    std::string _file_path;

    std::string _name;

    database_t _people;

    std::string _hash_value;

public:

    people_list();

    people_list(const std::string& name);

    /**
     * Loads this instance using the headline of a session-file
     * containing this list's name which is will be passed to
     * the loading-process. The hash of the given headline is then
     * compared with the current hash of this list.
     * 
     * @param session_headline headline of a session consisting of a list-name and that list's hash
     * @return hash-comparison result
     */
    bool load_and_check(const std::string& session_headline);

    /**
     * Compares a given hash with the current hash of this list
     * 
     * @return hash-comparison result
     */
    bool check(const std::string& hash) const;

    /**
     * @return the current hash of this list
     */
    const std::string& hash_value() const;

    /**
     * @return the current file-path of this list
     */
    const std::string& file_path() const;

    /**
     * @return cardinality of this list
     */
    size_t size() const;

    database_t::const_iterator cbegin() const;

    database_t::const_iterator cend() const;

    database_t::const_iterator find(const database_t::value_type& value);

    /**
     * Prints all elements in lexicographical order
     */
    void print() const;

    /**
     * Loads a list from a file in PEOPLE_LISTS_DIR having this name
     */
    void load(const std::string& name);

    /**
     * @return the name of this list
     */
    const std::string& name() const;
    
    /**
     * @return @code{true} when this list was loaded
     */
    bool loaded() const;

    virtual ~people_list();

};
