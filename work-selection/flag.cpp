#include "include/flag.hpp"

/** @file */

flag::flag()
    : _state(false) {

}

void flag::hoist() {
    this->_state = true;
}

bool flag::state() const {
    return this->_state;
}