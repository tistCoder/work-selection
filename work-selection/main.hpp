#pragma once


/** @file */

#include "mask_piece.hpp"
#include "session.hpp"
#include "tools.hpp"

#include <ctime>
#include <fstream>
#include <filesystem>
#include <iostream>
#include <random>
#include <string>

/** Handles the end of a selection-event **/
bool query_finished(session& sess);

/**
 * Enables printing of a selection-result using ostreams
 * 
 * @param os any ostream
 * @param res the selection_result which has to be printed
 */
std::ostream& operator<<(std::ostream& os, const selection_result& res);
