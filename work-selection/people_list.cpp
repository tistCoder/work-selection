#include "include/people_list.hpp"

/** @file */

people_list::people_list()
    : _file_path(), _name(), _people() {
}

people_list::people_list(const std::string& name)
    : _file_path(PEOPLE_LISTS_DIR + name), _name(name), _people() {
    this->load(name);
}

bool people_list::load_and_check(const std::string& session_headline) {
    //begin of name
    size_t bon = 0;
    for(; bon < session_headline.size(); bon++) {
        if(session_headline[bon] == '"') {
            bon++;
            break;
        }
    }
    //end of name
    size_t eon = bon;
    for(; eon < session_headline.size(); eon++) {
        if(session_headline[eon] == '"') {
            break;
        }
    }
    this->load(session_headline.substr(bon, eon-bon));
    return this->check(session_headline.substr(eon+2));
}

bool people_list::check(const std::string& hash) const {
    return hash == this->_hash_value;
}

const std::string& people_list::hash_value() const {
    return this->_hash_value;
}

const std::string& people_list::file_path() const {
    return this->_file_path;
}

size_t people_list::size() const {
    return this->_people.size();
}

database_t::const_iterator people_list::cbegin() const {
    return this->_people.cbegin();
}

database_t::const_iterator people_list::cend() const {
    return this->_people.cend();
}

database_t::const_iterator people_list::find(const database_t::value_type& value) {
    return this->_people.find(value);
}

void people_list::print() const {
    size_t count = 0;
    for(auto i = this->_people.cbegin(); i != this->_people.cend(); i++, count++) {
        std::cout << count << ": " << *i << std::endl;
    }
    return;
}

void people_list::load(const std::string& name) {
    //creates a suitable hash function in order to hash the internal set
    //so the interpreted list is hashed, not the bare file
    std::unique_ptr<Botan::HashFunction> hash_func(Botan::HashFunction::create(CHECKSUM_TYPE));
    std::string input = ""; //buffer
    this->_name = name;
    this->_file_path = PEOPLE_LISTS_DIR + name;
    std::ifstream pool_reader(this->_file_path);
    if (!pool_reader.is_open()) {
        fatal_error(MSG_ERROR_UNABLE_TO_LOAD_FILE(this->_file_path), CODE_OPEN_FAILURE);
    }
    //adding names to the pool
    while (std::getline(pool_reader, input, '\n')) {
        this->_people.insert(input);
        hash_func->update(input);
    }
    pool_reader.close();
    this->_loaded = true;
    this->_hash_value = Botan::hex_encode(hash_func->final());
    //in case it is empty
    if (this->_people.empty()) {
        std::cerr << "List is empty" << std::endl;
        return;
    }
    return;
}

const std::string& people_list::name() const {
    return this->_name;
}

bool people_list::loaded() const {
    return this->_loaded;
}

people_list::~people_list() {
    
}